let array = ["Kharkiv", "Kyiv", ["Borispol", "Irpin"], "Odesa", "Lviv", "Dnipro"];

function list(arr, domEl = document.body){
    let ul = document.createElement("ul");
    domEl.appendChild(ul);
    arr.map(function forMap(el, domEl) {
        if(Array.isArray(el)){
            list(el, document.querySelector("ul"));
        }
        else{
            let li = document.createElement("li");
            li.textContent = el;
            ul.appendChild(li);
        }
    });
}

list(array);
let p = document.createElement("p");
document.body.appendChild(p);
p.classList.add("number");

let num = 0;
let numNode = document.querySelector(".number");
numNode.style.color = "red";

numNode.textContent = num;
let timer = setInterval(() => {
    num++;
    numNode.textContent = num;
    if(num == 4){
        document.body.remove();
        clearInterval(timer);
    } 
}, 1000);
console.log()





